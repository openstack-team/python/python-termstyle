python-termstyle (0.1.10-7) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090627).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 22:11:22 +0100

python-termstyle (0.1.10-6) unstable; urgency=medium

  * Removed python3-setuptools from runtime dpends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Oct 2024 10:32:06 +0200

python-termstyle (0.1.10-5) unstable; urgency=medium

  * Do not runtime depends on python3-pkg-resource (Closes: #1083717).
  * Bump standards-version and debhelper-compat.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Oct 2024 09:43:43 +0200

python-termstyle (0.1.10-4) unstable; urgency=medium

  * Cleans better (Closes: #1048055).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 16:14:45 +0200

python-termstyle (0.1.10-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Removed Python 2 support (Closes: #938213).

 -- Thomas Goirand <zigo@debian.org>  Sat, 14 Sep 2019 10:30:14 +0200

python-termstyle (0.1.10-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.

  [ Thomas Goirand ]
  * Add missing dh-python build-depends.
  * Removed inactive uploaders.
  * Bumped debhelper to 10.
  * Removed Pre-Depends: dpkg (>= 1.15.6~).
  * Removed version of python-all b-d.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Apr 2018 17:32:07 +0200

python-termstyle (0.1.10-1) unstable; urgency=low

  * Initial release. (Closes: #742599)

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Mar 2014 18:22:46 +0800
